-- Views

CREATE VIEW StudentsFollowing AS (
  SELECT Students.login_name, Students.name, Students.programme, BelongToBranches.branch, Students.national_id
    FROM Students 
      LEFT JOIN BelongToBranches
        ON Students.login_name = BelongToBranches.student 
  );

CREATE VIEW FinishedCourses AS (
  SELECT Students.login_name, Courses.name, HasCompleted.grade, Courses.credits 
    FROM Students, HasCompleted, Courses
      WHERE HasCompleted.student = Students.login_name AND HasCompleted.course = Courses.code
  );

CREATE VIEW Registrations AS (
  SELECT * FROM (
  
    SELECT Students.login_name student, course, status 
      FROM Students, (SELECT student, course course, 'registered' AS status FROM IsRegisteredFor) Reg
        WHERE Students.login_name = Reg.student
    )
    UNION
    (
    SELECT Students.login_name student, course, status 
      FROM Students, (SELECT student, course course, 'waiting' AS status FROM IsWaitingFor) Wai
        WHERE Students.login_name = Wai.student
    )
  );

  
CREATE VIEW PassedCourses AS (
  SELECT Students.login_name, Courses.code, Courses.credits, HasCompleted.grade
    FROM Students, Courses, HasCompleted
      WHERE Students.login_name = HasCompleted.student AND HasCompleted.course = Courses.code
        AND HasCompleted.grade <> 'U'
);

-- Doesn't list a course if ANY student has passed it.
CREATE VIEW UnreadMandatory AS (
  SELECT DISTINCT * FROM (
    (
    SELECT S.login_name, Man.course 
      FROM StudentsFollowing S, IsMandatoryForProgrammes Man
        WHERE S.programme = Man.programme 
      )
      UNION
      (
    SELECT S.login_name student, Man.course 
      FROM StudentsFollowing S 
        NATURAL JOIN
         IsMandatoryForBranches Man
    ) 
  )
    WHERE course NOT IN (
            -- Get courses student has passed
            SELECT course 
              FROM HasCompleted
                WHERE login_name = HasCompleted.student
                  AND grade <> 'U'
            )
);

CREATE VIEW PathToGraduation AS (
  SELECT 
        student, NVL(total_credits, 0) total_credits, NVL(unpassed_mandatory, 0) unpassed_mandatory, 
        NVL(math_credits, 0) math_credits, NVL(research_credits, 0) research_credits, 
        NVL("#seminar_courses", 0) "#seminar_courses", 
      CASE 
        WHEN 
          (unpassed_mandatory IS NULL 
          AND branch IS NOT NULL
          AND math_credits >= 20 
          AND research_credits >= 10 
          AND "#seminar_courses" >= 1 
          AND recommended_credits >= 10) 
        THEN 'yes' 
        ELSE 'no'
      END can_graduate
      FROM
      --Get all students
    ( SELECT login_name student, branch
      FROM StudentsFollowing)
      NATURAL LEFT JOIN
    (--Number of credits per student
    SELECT login_name student, SUM(credits) total_credits
      FROM PassedCourses
    GROUP BY login_name
    )
    NATURAL LEFT JOIN
    ( --Mandatory yet to take
    SELECT login_name student, COUNT(course) unpassed_mandatory
      FROM UnreadMandatory
    GROUP BY login_name
    )
    NATURAL LEFT JOIN
    -- Credits for Math courses
    ( SELECT login_name student, SUM(credits) math_credits
      FROM PassedCourses Pas
      INNER JOIN HasClassifications Cla ON Pas.code = Cla.course
        WHERE classification = 'Math'
        GROUP BY login_name
    )
    NATURAL LEFT JOIN
      -- Credits for Research courses
    ( SELECT login_name student, SUM(credits) research_credits
      FROM PassedCourses Pas
      INNER JOIN HasClassifications Cla ON Pas.code = Cla.course
        WHERE classification = 'Research'
        GROUP BY login_name
    )
    NATURAL LEFT JOIN
    -- Number of Seminar courses
    ( SELECT login_name student, COUNT(Pas.code) "#seminar_courses"
      FROM PassedCourses Pas
      INNER JOIN HasClassifications Cla ON Pas.code = Cla.course
        WHERE classification = 'Seminar'
        GROUP BY login_name
    )
    NATURAL LEFT JOIN 
    -- Get credtis passed for recommended courses (doesn't get displayed, but is counted)
    ( SELECT login_name student, SUM(credits) recommended_credits
      FROM PassedCourses Pas
          INNER JOIN IsRecommendedForBranches Rec ON Pas.code = Rec.course
        GROUP BY login_name
    )
    
);