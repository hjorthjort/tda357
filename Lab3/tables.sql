begin
for c in (select table_name from user_tables) loop
execute immediate ('drop table '||c.table_name||' cascade constraints');
end loop;
end;
/
begin
for c in (select * from user_objects) loop
execute immediate ('drop '||c.object_type||' '||c.object_name);
end loop;
end;
/

-- Departments & Programmes

CREATE TABLE Departments (
  abbreviation CHAR(5) PRIMARY KEY,
  name VARCHAR(255) UNIQUE
  );

CREATE TABLE StudyProgrammes(
  name VARCHAR(255) PRIMARY KEY,
  abbreviation CHAR(4)
  );

CREATE TABLE HostsProgrammes (
  department REFERENCES Departments ON DELETE CASCADE,
  programme REFERENCES StudyProgrammes ON DELETE CASCADE
  );

-- Courses

CREATE TABLE Courses (
  code  CHAR(6) NOT NULL PRIMARY KEY,
  credits FLOAT CONSTRAINT PositiveCredits CHECK (credits > 0),
  name  VARCHAR(255),
  department CONSTRAINT ExistingDepartment REFERENCES Departments ON DELETE SET NULL
  );
  
CREATE TABLE LimitedCourses (
  code PRIMARY KEY REFERENCES Courses ON DELETE CASCADE,
  max_students INT CHECK (max_students >= 0)
  );
  
CREATE TABLE IsPrereqFor (
  course REFERENCES Courses ON DELETE CASCADE,
  prereq REFERENCES Courses ON DELETE CASCADE,
  PRIMARY KEY (course, prereq)
  );
  
CREATE TABLE CourseClassifications (
  classification_name VARCHAR(255) PRIMARY KEY
  );
  
CREATE TABLE HasClassifications(
  course REFERENCES Courses ON DELETE CASCADE,
  classification REFERENCES CourseClassifications ON DELETE CASCADE
  );
  
-- Branch

CREATE TABLE Branches(
  programme REFERENCES StudyProgrammes ON DELETE CASCADE,
  name VARCHAR(255),
  PRIMARY KEY(programme, name)
  );
  
-- Mandaoty & Recommended
CREATE TABLE IsMandatoryForProgrammes(
  course REFERENCES Courses ON DELETE CASCADE,
  programme REFERENCES StudyProgrammes ON DELETE CASCADE,
  PRIMARY KEY(course, programme)
  );
CREATE TABLE IsMandatoryForBranches(
  course REFERENCES Courses ON DELETE CASCADE,
  programme VARCHAR(255),
  branch VARCHAR(255),
  PRIMARY KEY(course, programme, branch),
  FOREIGN KEY(programme, branch) REFERENCES Branches ON DELETE CASCADE
  );

CREATE TABLE IsRecommendedForBranches(
  course REFERENCES Courses ON DELETE CASCADE,
  programme VARCHAR(255),
  branch VARCHAR(255),
  PRIMARY KEY(course, programme, branch),
  FOREIGN KEY(programme, branch) REFERENCES Branches ON DELETE CASCADE
  );
  
-- Students
CREATE TABLE Students(
  login_name VARCHAR(255) PRIMARY KEY,
  name VARCHAR(511),
  programme REFERENCES StudyProgrammes ON DELETE SET NULL,
  national_id CHAR(12) UNIQUE,
  UNIQUE (login_name, programme)
  );

CREATE TABLE BelongToBranches(
  student PRIMARY KEY REFERENCES Students,
  programme VARCHAR(255),
  branch VARCHAR(255),
  CONSTRAINT ExistingBranch FOREIGN KEY (programme, branch) REFERENCES Branches(programme, name) ON DELETE CASCADE,
  CONSTRAINT CorrectProgramme FOREIGN KEY (student, programme) REFERENCES Students(login_name, programme) ON DELETE CASCADE
  );
  
-- Students & courses
CREATE TABLE IsWaitingFor (
  student REFERENCES Students ON DELETE CASCADE,
  course REFERENCES LimitedCourses ON DELETE CASCADE,
  sinceDate TIMESTAMP (9),
  PRIMARY KEY(student, course),
  CONSTRAINT NoSamePlaceInLine UNIQUE (course, sinceDate)
  );

CREATE TABLE IsRegisteredFor (
  student REFERENCES Students ON DELETE CASCADE,
  course REFERENCES Courses ON DELETE CASCADE,
  PRIMARY KEY(student, course)
  );
  
CREATE TABLE HasCompleted (
  student REFERENCES Students ON DELETE SET NULL, -- Since we might want to keep grading data!
  course REFERENCES Courses ON DELETE SET NULL,
  grade CHAR(1) CONSTRAINT ValidGrade CHECK (grade IN ('U', '3', '4', '5')),
  PRIMARY KEY(student, course)
  );
