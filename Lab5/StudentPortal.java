import java.sql.*; // JDBC stuff.
import java.io.*;  // Reading user input.

public class StudentPortal
{
    private static final int HEADER_SIZE = 30;
    /* This is the driving engine of the program. It parses the
     * command-line arguments and calls the appropriate methods in
     * the other classes.
     *
     * You should edit this file in two ways:
     * 	1) 	Insert your database username and password (no @medic1!)
     *		in the proper places.
     *	2)	Implement the three functions getInformation, registerStudent
     *		and unregisterStudent.
     */
    public static void main(String[] args)
    {
        if (args.length == 1) {
            try {
                DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
                String url = "jdbc:oracle:thin:@db.student.chalmers.se:1521/kingu.ita.chalmers.se";
                String userName = "htda357_061"; // Your username goes here!
                String password = "x0a71576"; // Your password goes here!
                Connection conn = DriverManager.getConnection(url,userName,password);

                String student = args[0]; // This is the identifier for the student.
                BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Welcome!");
                while(true) {
                    System.out.println("Please choose a mode of operation:");
                    System.out.print("? > ");
                    String mode = input.readLine();
                    if ((new String("information")).startsWith(mode.toLowerCase())) {
                        /* Information mode */
                        getInformation(conn, student);
                    } else if ((new String("register")).startsWith(mode.toLowerCase())) {
                        /* Register student mode */
                        System.out.print("Register for what course? > ");
                        String course = input.readLine();
                        registerStudent(conn, student, course);
                    } else if ((new String("unregister")).startsWith(mode.toLowerCase())) {
                        /* Unregister student mode */
                        System.out.print("Unregister from what course? > ");
                        String course = input.readLine();
                        unregisterStudent(conn, student, course);
                    } else if ((new String("quit")).startsWith(mode.toLowerCase())) {
                        System.out.println("Goodbye!");
                        break;
                    } else {
                        System.out.println("Unknown argument, please choose either information, register, unregister or quit!");
                        continue;
                    }
                }
                conn.close();
            } catch (SQLException e) {
                System.err.println(e);
                System.exit(2);
            } catch (IOException e) {
                System.err.println(e);
                System.exit(2);
            }
        } else {
            System.err.println("Wrong number of arguments");
            System.exit(3);
        }
    }

    static void getInformation(Connection conn, String student) {
        try {
            Statement select = conn.createStatement();
            // Print general info about student
            String generalInfoQuery = 
                "SELECT login_name, name, programme, branch, national_id " +
                "FROM StudentsFollowing " +
                "WHERE login_name = '" + student + "'";
            ResultSet generalInfoSet = select.executeQuery(generalInfoQuery);
            // If database invariants hold, we have at most returned 1 row
            if (generalInfoSet.next()) {
                String login_name = student;
                String name = generalInfoSet.getString(2);
                String programme = generalInfoSet.getString(3);
                String branch = generalInfoSet.getString(4);
                String national_id = generalInfoSet.getString(5);
                printStudentInfo(login_name, name, programme, branch, national_id);
            } else {
                return ;
            }

            //List finished courses
            String finishedCoursesQuery =
                "SELECT code, name, grade " +
                "FROM FinishedCourses " +
                "WHERE login_name = '" + student + "'";
            ResultSet finishedCoursesSet = select.executeQuery(finishedCoursesQuery);

            printHeader("Finished courses");
            System.out.println(String.format("%-10s%-30s%-4s", "code", "name", "grade"));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 45; i++)
                sb.append("-");
            System.out.println(sb.toString());
            while (finishedCoursesSet.next()) {
                String code = finishedCoursesSet.getString(1);
                String name = finishedCoursesSet.getString(2);
                String grade = finishedCoursesSet.getString(3);
                printCourse(code, name, grade);
            }

            //List courses registered for
            String registeredCoursesQuery =
                "SELECT Registrations.course, Courses.name, Registrations.status " +
                "FROM Registrations, Courses " +
                "WHERE student = '" + student + "'" + 
                "AND Registrations.course = Courses.code";
            ResultSet registeredCoursesSet = select.executeQuery(registeredCoursesQuery);

            System.out.print("\n\n");
            printHeader("Registered courses");
            System.out.println(String.format("%-10s%-30s%-4s", "code", "name", "status"));
            sb.setLength(0);
            for (int i = 0; i < 45; i++)
                sb.append("-");
            System.out.println(sb.toString());

            while (registeredCoursesSet.next()) {
                String code = registeredCoursesSet.getString(1);
                String name = registeredCoursesSet.getString(2);
                String status = registeredCoursesSet.getString(3);
                if (status.equals("waiting")){
                    Statement tempStatement = conn.createStatement();
                    ResultSet place = tempStatement.executeQuery(
                            "SELECT positionInQueue " +
                            "FROM CourseQueuePositions " +
                            "WHERE student = '" + student + "'" +
                            "AND course = '" + code + "'"
                            );
                    place.next();
                    status += ", #" + place.getInt(1);
                }
                printCourse(code, name, status);
            }

            //List if student can graduate
            String canGraduateQuery =
                "SELECT can_graduate FROM PathToGraduation " +
                "WHERE student = '" + student + "'";
            ResultSet canGraduateResultSet = select.executeQuery(canGraduateQuery);

            System.out.print("\n\n");
            printHeader("Can you graduate?");
            sb.setLength(0);
            canGraduateResultSet.next();
            String canGraduate = canGraduateResultSet.getString(1);
            printHeader(canGraduate);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void printStudentInfo(String loginName, String name, 
            String programme, String branch, String nationalId) {
        printHeader("General Student Info");
        System.out.println(String.format("Name: %s\nCid: %s\nNationa id: %s\n",
                    name, loginName, nationalId));
        System.out.println(String.format("Programme: %s\nBranch: %s\n\n", 
                    programme, branch == null ? "None" : branch));
    }

    private static void printCourse(String code, String name, String info) {
        if (name.length() > 28) {
            name = name.substring(0,25) + "...";
        }
        System.out.println(String.format("%-10s%-30s%-4s", code, name, info));
    }

    private static void printHeader(String title) {
        StringBuilder sb = new StringBuilder();
        int filling = (HEADER_SIZE - (title.length() + 2)) / 2;
        for (int i = 0; i < filling; i++)
            sb.append("*");
        sb.append(" ");
        sb.append(title.toUpperCase());
        sb.append(" ");
        for (int i = 0; i < filling; i++)
            sb.append("*");
        System.out.println(sb.toString());
    }


    static void registerStudent(Connection conn, String student, String course) {
        try {
            course = course.toUpperCase();
            Statement stmt = conn.createStatement();
            String insert = "INSERT INTO Registrations (student, course) VALUES ('" + student + "', '" + course + "')";
            stmt.executeUpdate(insert);
            printHeader("Successfully registered");
        } catch (SQLException e) {
            switch (e.getErrorCode()) {
                case 1401: //Inserted value too long
                case 2291: //No such course found
                    printHeader("No such course");
                    break;
                case 20002: //Student already registered
                    printHeader("Student is already registered");
                    break;
                case 20003: //Doesn't have prerequisites
                    printHeader("Student lacks prerequisites");
                    break;
                default:
                    e.printStackTrace();
            }
        }
    }

    static void unregisterStudent(Connection conn, String student, String course) {
        try {
            course = course.toUpperCase();
            Statement stmt = conn.createStatement();
            String delete = "DELETE FROM Registrations WHERE student = '" + student + "' AND course = '" + course + "'";
            stmt.executeUpdate(delete);
            printHeader("Successfully unregistered");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
