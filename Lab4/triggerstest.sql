-- Cascading means this deletion is enough
DELETE FROM Courses WHERE code = 'APA123' OR code = 'SOC999' OR code = 'HUM101';
    
DELETE FROM Students WHERE login_name = 'DonkeyKong' OR login_name = 'JaneGoodall' 
  OR login_name = 'MaryShelley' OR login_name = 'Chico';


-- Create some courses
INSERT INTO Courses 
  VALUES
    ('APA123', 7.5, 'Monkey Science', 'CE');

INSERT INTO Courses 
  VALUES
    ('SOC999', 7.5, 'Social Science', 'CS');
    
INSERT INTO Courses 
  VALUES
    ('HUM101', 7.5, 'Humanoid Science', 'SE');

INSERT INTO LimitedCourses 
  VALUES
    ('HUM101', 2);
    
INSERT INTO IsPrereqFor
  VALUES
    ('HUM101', 'APA123');
INSERT INTO IsPrereqFor
  VALUES
    ('HUM101', 'SOC999');


-- Create some students
INSERT INTO Students
  VALUES
    ('DonkeyKong', 'Donkey Kong', 'Informationsteknik', '11110107APAB');
INSERT INTO Students
  VALUES
    ('JaneGoodall', 'Jane Goodall', 'Informationsteknik', '29910107APAB');
INSERT INTO Students
  VALUES
    ('MaryShelley', 'Mary Shelley', 'Informationsteknik', '19919107APAB');
INSERT INTO Students
  VALUES
    ('Chico', 'Cheeky Monkey', 'Informationsteknik', '11110107A1AB');

-- Make some of them pass prereqs

INSERT INTO HasCompleted(student, course, grade) VALUES ('DonkeyKong', 'APA123', 3);
INSERT INTO HasCompleted(student, course, grade) VALUES ('JaneGoodall', 'APA123', 5);
INSERT INTO HasCompleted(student, course, grade) VALUES ('MaryShelley', 'APA123', 4);

INSERT INTO HasCompleted(student, course, grade) VALUES ('DonkeyKong', 'SOC999', 3);
INSERT INTO HasCompleted(student, course, grade) VALUES ('JaneGoodall', 'SOC999', 5);
INSERT INTO HasCompleted(student, course, grade) VALUES ('MaryShelley', 'SOC999', 4);

-- Humanoid science has two spots, and students must take two prerequisite 
-- courses before they can register  

set serveroutput on;


-- **********************************
-- **** TEST REGISTERING ************
-- **********************************

BEGIN
  -- FAILS! Has already passed    
  INSERT INTO Registrations (student, course) VALUES ('DonkeyKong', 'APA123');
  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
  IF SQLCODE =-20001 THEN
    dbms_output.put_line('Proper fail: Donkey Kong to APA123, already passed');
  ELSE
      dbms_output.put_line(SQLCODE);

    RAISE_APPLICATION_ERROR(-20099, 'Failed test');
  END IF;
END;
  /
-- Works: Not yet registered
INSERT INTO Registrations (student, course) VALUES ('Chico', 'APA123');

BEGIN
  -- FAILS: Already registered
  INSERT INTO Registrations (student, course) VALUES ('Chico', 'APA123');
  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
  IF SQLCODE =-20002 THEN
  dbms_output.put_line('Proper fail: Chico to APA123, already registered');
  ELSE
    RAISE_APPLICATION_ERROR(-20099, 'Failed test');
  END IF;
END;
  /
  
BEGIN
  -- FAILS: Lacks prerequisites
  INSERT INTO Registrations (student, course) VALUES ('Chico', 'HUM101');
  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
  IF SQLCODE =-20003 THEN
  dbms_output.put_line('Proper fail: Chico to HUM101, lack prereq');
  ELSE
    RAISE_APPLICATION_ERROR(-20099, 'Failed test');
  END IF;
END;
  /


--Should work: Last one should be set to waiting
INSERT INTO Registrations (student, course) VALUES ('DonkeyKong', 'HUM101');
INSERT INTO Registrations (student, course) VALUES ('JaneGoodall', 'HUM101');
INSERT INTO Registrations (student, course) VALUES ('MaryShelley', 'HUM101');

BEGIN
  -- FAILS: Already registered 
  INSERT INTO Registrations (student, course) VALUES ('JaneGoodall', 'HUM101');
  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
  IF SQLCODE =-20002 THEN
  dbms_output.put_line('Proper fail: JaneGoodall to HUM101, already reg');
  ELSE
    RAISE_APPLICATION_ERROR(-20099, 'Failed test');
  END IF;
END;
  /
-- Both should fail: one is registered, one is waiting
BEGIN
  -- FAILS: Already waiting 
  INSERT INTO Registrations (student, course) VALUES ('MaryShelley', 'HUM101');  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
  IF SQLCODE =-20002 THEN
  dbms_output.put_line('Proper fail: MaryShelley to HUM101, already waiting');
  ELSE
    RAISE_APPLICATION_ERROR(-20099, 'Failed test');
  END IF;
END;
/

-- **********************************
-- **** TEST UNREGISTERING **********
-- **********************************

DECLARE
oldRegistered Int;
newRegistered Int;
oldDonkey Int;
newDonkey Int;
donkeyStatus VARCHAR(10);
oldMaryStatus VARCHAR(10);
newMaryStatus VARCHAR(10);
BEGIN
  SELECT COUNT(*) INTO oldRegistered FROM Registrations WHERE course = 'HUM101' AND status = 'registered';
  SELECT COUNT(*) INTO oldDonkey FROM Registrations WHERE student = 'DonkeyKong' AND course = 'HUM101';
    SELECT status INTO donkeyStatus FROM Registrations WHERE student = 'DonkeyKong' AND course = 'HUM101';
  SELECT status INTO oldMaryStatus FROM Registrations WHERE student = 'MaryShelley' AND course = 'HUM101';

  IF oldMaryStatus <> 'waiting' OR oldDonkey <> 1 OR donkeyStatus <> 'registered' THEN
    RAISE_APPLICATION_ERROR(-20098, 'Test conditions not set up properly');
  END IF;
  
  DELETE FROM Registrations WHERE course = 'HUM101' AND student = 'DonkeyKong';

  SELECT COUNT(*) INTO newRegistered FROM Registrations WHERE course = 'HUM101' AND status = 'registered';
  SELECT COUNT(*) INTO newDonkey FROM Registrations WHERE student = 'DonkeyKong' AND course = 'HUM101';
  SELECT status INTO newMaryStatus FROM Registrations WHERE student = 'MaryShelley' AND course = 'HUM101';
  
  -- There where students waiting, so the number of registered should be the same before and after
  IF oldRegistered <> newRegistered THEN
    RAISE_APPLICATION_ERROR(-20097, 'Failed test: registered student number reduced');
  END IF;
  
  -- Donkey should be removed from the Registrations view
  IF newDonkey <> 0 THEN
    RAISE_APPLICATION_ERROR(-20096, 'Failed test: DonkeyKong still registered');
  END IF;
  
  -- Mary should be registered
  IF newMaryStatus <> 'registered' THEN
    RAISE_APPLICATION_ERROR(-20095, 'Failed test: registered student number reduced');
  END IF;
  dbms_output.put_line('All tests on deleting passed');
END;
/
  