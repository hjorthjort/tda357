begin
for c in (select table_name from user_tables) loop
execute immediate ('drop table '||c.table_name||' cascade constraints');
end loop;
end;
/
begin
for c in (select * from user_objects) loop
execute immediate ('drop '||c.object_type||' '||c.object_name);
end loop;
end;
/
--*************************************
--************  TABLES  ***************
--*************************************
-- Departments & Programmes

CREATE TABLE Departments (
  abbreviation CHAR(5) PRIMARY KEY,
  name VARCHAR(255) UNIQUE
  );

CREATE TABLE StudyProgrammes(
  name VARCHAR(255) PRIMARY KEY,
  abbreviation CHAR(4)
  );

CREATE TABLE HostsProgrammes (
  department REFERENCES Departments ON DELETE CASCADE,
  programme REFERENCES StudyProgrammes ON DELETE CASCADE
  );

-- Courses

CREATE TABLE Courses (
  code  CHAR(6) NOT NULL PRIMARY KEY,
  credits FLOAT CONSTRAINT PositiveCredits CHECK (credits > 0),
  name  VARCHAR(255),
  department CONSTRAINT ExistingDepartment REFERENCES Departments ON DELETE SET NULL
  );
  
CREATE TABLE LimitedCourses (
  code PRIMARY KEY REFERENCES Courses ON DELETE CASCADE,
  max_students INT CHECK (max_students >= 0)
  );
  
CREATE TABLE IsPrereqFor (
  course REFERENCES Courses ON DELETE CASCADE,
  prereq REFERENCES Courses ON DELETE CASCADE,
  PRIMARY KEY (course, prereq)
  );
  
CREATE TABLE CourseClassifications (
  classification_name VARCHAR(255) PRIMARY KEY
  );
  
CREATE TABLE HasClassifications(
  course REFERENCES Courses ON DELETE CASCADE,
  classification REFERENCES CourseClassifications ON DELETE CASCADE
  );
  
-- Branch

CREATE TABLE Branches(
  programme REFERENCES StudyProgrammes ON DELETE CASCADE,
  name VARCHAR(255),
  PRIMARY KEY(programme, name)
  );
  
-- Mandaoty & Recommended
CREATE TABLE IsMandatoryForProgrammes(
  course REFERENCES Courses ON DELETE CASCADE,
  programme REFERENCES StudyProgrammes ON DELETE CASCADE,
  PRIMARY KEY(course, programme)
  );
CREATE TABLE IsMandatoryForBranches(
  course REFERENCES Courses ON DELETE CASCADE,
  programme VARCHAR(255),
  branch VARCHAR(255),
  PRIMARY KEY(course, programme, branch),
  FOREIGN KEY(programme, branch) REFERENCES Branches ON DELETE CASCADE
  );

CREATE TABLE IsRecommendedForBranches(
  course REFERENCES Courses ON DELETE CASCADE,
  programme VARCHAR(255),
  branch VARCHAR(255),
  PRIMARY KEY(course, programme, branch),
  FOREIGN KEY(programme, branch) REFERENCES Branches ON DELETE CASCADE
  );
  
-- Students
CREATE TABLE Students(
  login_name VARCHAR(255) PRIMARY KEY,
  name VARCHAR(511),
  programme REFERENCES StudyProgrammes ON DELETE SET NULL,
  national_id CHAR(12) UNIQUE,
  UNIQUE (login_name, programme)
  );

CREATE TABLE BelongToBranches(
  student PRIMARY KEY REFERENCES Students,
  programme VARCHAR(255),
  branch VARCHAR(255),
  CONSTRAINT ExistingBranch FOREIGN KEY (programme, branch) REFERENCES Branches(programme, name) ON DELETE CASCADE,
  CONSTRAINT CorrectProgramme FOREIGN KEY (student, programme) REFERENCES Students(login_name, programme) ON DELETE CASCADE
  );
  
-- Students & courses
CREATE TABLE IsWaitingFor (
  student REFERENCES Students ON DELETE CASCADE,
  course REFERENCES LimitedCourses ON DELETE CASCADE,
  sinceDate TIMESTAMP (9),
  PRIMARY KEY(student, course),
  CONSTRAINT NoSamePlaceInLine UNIQUE (course, sinceDate)
  );

CREATE TABLE IsRegisteredFor (
  student REFERENCES Students ON DELETE CASCADE,
  course REFERENCES Courses ON DELETE CASCADE,
  PRIMARY KEY(student, course)
  );
  
CREATE TABLE HasCompleted (
  student REFERENCES Students ON DELETE CASCADE,
  course REFERENCES Courses ON DELETE CASCADE,
  grade CHAR(1) CONSTRAINT ValidGrade CHECK (grade IN ('U', '3', '4', '5')),
  PRIMARY KEY(student, course)
  );


--*******************************************
--*********  VIEWS    ***********************
--*******************************************

CREATE VIEW StudentsFollowing AS (
  SELECT Students.login_name, Students.name, Students.programme, BelongToBranches.branch, Students.national_id
    FROM Students 
      LEFT JOIN BelongToBranches
        ON Students.login_name = BelongToBranches.student 
  );

CREATE VIEW FinishedCourses AS (
  SELECT Students.login_name, Courses.code, Courses.name, HasCompleted.grade, Courses.credits 
    FROM Students, HasCompleted, Courses
      WHERE HasCompleted.student = Students.login_name AND HasCompleted.course = Courses.code
  );

CREATE VIEW Registrations AS (
  SELECT * FROM (
  
    SELECT Students.login_name student, course, status 
      FROM Students, (SELECT student, course course, 'registered' AS status FROM IsRegisteredFor) Reg
        WHERE Students.login_name = Reg.student
    )
    UNION
    (
    SELECT Students.login_name student, course, status 
      FROM Students, (SELECT student, course course, 'waiting' AS status FROM IsWaitingFor) Wai
        WHERE Students.login_name = Wai.student
    )
  );

  
CREATE VIEW PassedCourses AS (
  SELECT Students.login_name, Courses.code, Courses.credits, HasCompleted.grade
    FROM Students, Courses, HasCompleted
      WHERE Students.login_name = HasCompleted.student AND HasCompleted.course = Courses.code
        AND HasCompleted.grade <> 'U'
);

-- Doesn't list a course if ANY student has passed it.
CREATE VIEW UnreadMandatory AS (
  SELECT DISTINCT * FROM (
    (
    SELECT S.login_name, Man.course 
      FROM StudentsFollowing S, IsMandatoryForProgrammes Man
        WHERE S.programme = Man.programme 
      )
      UNION
      (
    SELECT S.login_name student, Man.course 
      FROM StudentsFollowing S 
        NATURAL JOIN
         IsMandatoryForBranches Man
    ) 
  )
    WHERE course NOT IN (
            -- Get courses student has passed
            SELECT course 
              FROM HasCompleted
                WHERE login_name = HasCompleted.student
                  AND grade <> 'U'
            )
);

CREATE VIEW PathToGraduation AS (
  SELECT 
        student, NVL(total_credits, 0) total_credits, NVL(unpassed_mandatory, 0) unpassed_mandatory, 
        NVL(math_credits, 0) math_credits, NVL(research_credits, 0) research_credits, 
        NVL("#seminar_courses", 0) "#seminar_courses", 
      CASE 
        WHEN 
          (unpassed_mandatory IS NULL 
          AND branch IS NOT NULL
          AND math_credits >= 20 
          AND research_credits >= 10 
          AND "#seminar_courses" >= 1 
          AND recommended_credits >= 10) 
        THEN 'yes' 
        ELSE 'no'
      END can_graduate
      FROM
      --Get all students
    ( SELECT login_name student, branch
      FROM StudentsFollowing)
      NATURAL LEFT JOIN
    (--Number of credits per student
    SELECT login_name student, SUM(credits) total_credits
      FROM PassedCourses
    GROUP BY login_name
    )
    NATURAL LEFT JOIN
    ( --Mandatory yet to take
    SELECT login_name student, COUNT(course) unpassed_mandatory
      FROM UnreadMandatory
    GROUP BY login_name
    )
    NATURAL LEFT JOIN
    -- Credits for Math courses
    ( SELECT login_name student, SUM(credits) math_credits
      FROM PassedCourses Pas
      INNER JOIN HasClassifications Cla ON Pas.code = Cla.course
        WHERE classification = 'Math'
        GROUP BY login_name
    )
    NATURAL LEFT JOIN
      -- Credits for Research courses
    ( SELECT login_name student, SUM(credits) research_credits
      FROM PassedCourses Pas
      INNER JOIN HasClassifications Cla ON Pas.code = Cla.course
        WHERE classification = 'Research'
        GROUP BY login_name
    )
    NATURAL LEFT JOIN
    -- Number of Seminar courses
    ( SELECT login_name student, COUNT(Pas.code) "#seminar_courses"
      FROM PassedCourses Pas
      INNER JOIN HasClassifications Cla ON Pas.code = Cla.course
        WHERE classification = 'Seminar'
        GROUP BY login_name
    )
    NATURAL LEFT JOIN 
    -- Get credtis passed for recommended courses (doesn't get displayed, but is counted)
    ( SELECT login_name student, SUM(credits) recommended_credits
      FROM PassedCourses Pas
          INNER JOIN IsRecommendedForBranches Rec ON Pas.code = Rec.course
        GROUP BY login_name
    )
    
);

CREATE VIEW CourseQueuePositions AS (
  SELECT student, course, ROW_NUMBER() OVER (PARTITION BY course ORDER BY sinceDate) AS positionInQueue
    FROM IsWaitingFor
);

--*******************************************
--*********  INSERTS  ***********************
--*******************************************


-- Create Departments and programmes
INSERT INTO Departments 
  VALUES
    ('CS', 'Comupter Science');
INSERT INTO Departments 
  VALUES
    ('CE', 'Comupter Engineering');
INSERT INTO Departments 
  VALUES
    ('SE', 'Software Engineering');

--Create courses
INSERT INTO Courses
  VALUES
    ('TDA357', 10, 'Databases', 'CS');
INSERT INTO LimitedCourses 
  VALUES
    ('TDA357', 127);
INSERT INTO Courses 
  VALUES
    ('TIN070', 7.5, 'Algorithms', 'CE');
INSERT INTO LimitedCourses 
  VALUES
    ('TIN070', 30);
INSERT INTO Courses
  VALUES
    ('DAT216', 6, 'Graphical Interfaces Design and Construction', 'CE');
INSERT INTO Courses
  VALUES
    ('TDA545', 7.5, 'Object Oriented Programming', 'SE');
INSERT INTO Courses
  VALUES
    ('TDA550', 15, 'Object Oriented Programming', 'SE');
     
    
--Classifications
INSERT INTO CourseClassifications
  VALUES
    ('Math');
INSERT INTO CourseClassifications
  VALUES
    ('Programming');
INSERT INTO CourseClassifications
  VALUES
    ('Research');
INSERT INTO CourseClassifications
  VALUES
    ('Seminar');
    
INSERT INTO HasClassifications
  VALUES
    ('TDA357', 'Programming');
INSERT INTO HasClassifications
  VALUES
    ('TDA357', 'Research');
INSERT INTO HasClassifications
  VALUES
    ('TDA357', 'Seminar');
INSERT INTO HasClassifications
  VALUES
    ('TDA357', 'Math');
INSERT INTO HasClassifications
  VALUES
    ('TIN070', 'Programming');
 INSERT INTO HasClassifications
  VALUES
    ('TIN070', 'Math');

INSERT INTO HasClassifications
  VALUES
    ('TDA545', 'Programming');
INSERT INTO HasClassifications
  VALUES
    ('TDA545', 'Math');
    
-- Prerequisites

INSERT INTO IsPrereqFor
  VALUES
    ('TDA545', 'TDA550');
INSERT INTO IsPrereqFor
  VALUES
    ('TDA545', 'TDA357');
    
-- StudyProgramme
INSERT INTO StudyProgrammes
  VALUES
    ('Informationsteknik', 'TKIT');
 INSERT INTO StudyProgrammes
  VALUES
    ('Datateknik', 'TKDT');
INSERT INTO StudyProgrammes
  VALUES
    ('Datavetenskap', 'DVDA');
INSERT INTO HostsProgrammes
  VALUES
    ('CS', 'Datavetenskap');
INSERT INTO HostsProgrammes
  VALUES
    ('CE', 'Datavetenskap');
INSERT INTO HostsProgrammes
  VALUES
    ('SE', 'Informationsteknik');
INSERT INTO HostsProgrammes
  VALUES
    ('SE', 'Datateknik');

--Branches
INSERT INTO Branches
  VALUES
    ('Informationsteknik', 'Interaction Design');
INSERT INTO Branches
  VALUES
    ('Informationsteknik', 'Software Engineering');
INSERT INTO Branches
  VALUES
    ('Datateknik', 'Interaction Design');
INSERT INTO Branches
  VALUES
    ('Datavetenskap', 'Turing Machines');

-- Recommended & Mandatory
INSERT INTO IsMandatoryForProgrammes
  VALUES
    ('TDA545', 'Informationsteknik');
    INSERT INTO IsMandatoryForProgrammes
VALUES
    ('TDA545', 'Datateknik');
    INSERT INTO IsMandatoryForProgrammes
VALUES
    ('TDA550', 'Datateknik');
     
INSERT INTO IsMandatoryForProgrammes
  VALUES
    ('TDA545', 'Datavetenskap');
INSERT INTO IsMandatoryForProgrammes
  VALUES
    ('TIN070', 'Datavetenskap');

INSERT INTO IsMandatoryForBranches
  VALUES
    ('DAT216', 'Informationsteknik', 'Interaction Design');
INSERT INTO IsMandatoryForBranches
  VALUES
    ('TDA550', 'Informationsteknik', 'Interaction Design');
INSERT INTO IsMandatoryForBranches
  VALUES
    ('TIN070', 'Datavetenskap', 'Turing Machines');

INSERT INTO IsRecommendedForBranches
  VALUES
    ('TDA357', 'Informationsteknik', 'Interaction Design');
INSERT INTO IsRecommendedForBranches
  VALUES
    ('DAT216', 'Datateknik', 'Interaction Design');    

-- Student
INSERT INTO Students
  VALUES
    ('hjorthjort', 'Rikard Hjort', 'Informationsteknik', '19910107APAB');
INSERT INTO Students
  VALUES
    ('apabepa', 'Rikard Hjort', 'Informationsteknik', '20110011APAB');
INSERT INTO Students
  VALUES
    ('foobar', 'Robert Gjort', 'Informationsteknik', '19810107APAB');
INSERT INTO Students
  VALUES
    ('databepa', 'Apa Bepa', 'Datateknik', '19910117APAB');
INSERT INTO Students
  VALUES
    ('datahjort', 'Rebecka Hjort', 'Datateknik', '19901117APAB');
INSERT INTO Students
  VALUES
    ('databar', 'Foo Bar', 'Datateknik', '17101117APAB');
INSERT INTO Students
  VALUES
    ('turing12', 'Alan Turing', 'Datavetenskap', '19120623ABAC');

-- Belonging to brnaches

INSERT INTO BelongToBranches
  VALUES
    ('hjorthjort', 'Informationsteknik', 'Interaction Design');
INSERT INTO BelongToBranches
  VALUES
    ('apabepa', 'Informationsteknik', 'Software Engineering');
INSERT INTO BelongToBranches
  VALUES
    ('foobar', 'Informationsteknik', 'Interaction Design');
INSERT INTO BelongToBranches
  VALUES
    ('turing12', 'Datavetenskap', 'Turing Machines');


--Taking Courses

INSERT INTO IsWaitingFor
  VALUES
    ('turing12', 'TDA357', CURRENT_TIMESTAMP(9));
INSERT INTO IsWaitingFor
  VALUES
    ('hjorthjort', 'TDA357', CURRENT_TIMESTAMP(9));
INSERT INTO IsWaitingFor
  VALUES
    ('databar', 'TDA357', CURRENT_TIMESTAMP(9));
INSERT INTO IsWaitingFor
  VALUES
    ('foobar', 'TDA357', CURRENT_TIMESTAMP(9));

INSERT INTO IsWaitingFor
  VALUES
    ('turing12', 'TIN070', CURRENT_TIMESTAMP(9));
INSERT INTO IsWaitingFor
  VALUES
    ('hjorthjort', 'TIN070', CURRENT_TIMESTAMP(9));
INSERT INTO IsWaitingFor
  VALUES
    ('databar', 'TIN070', CURRENT_TIMESTAMP(9));
INSERT INTO IsWaitingFor
  VALUES
    ('foobar', 'TIN070', CURRENT_TIMESTAMP(9));


INSERT INTO IsRegisteredFor
  VALUES
    ('turing12', 'TDA550');
INSERT INTO IsRegisteredFor
  VALUES
    ('hjorthjort', 'TDA550');
INSERT INTO IsRegisteredFor
  VALUES
    ('databar', 'TIN070');
INSERT INTO IsRegisteredFor
  VALUES
    ('foobar', 'TIN070');

INSERT INTO HasCompleted
  VALUES
    ('turing12', 'DAT216', '5');
INSERT INTO HasCompleted
  VALUES
    ('turing12', 'TIN070', '4');
INSERT INTO HasCompleted 
  VALUES 
    ('hjorthjort', 'TIN070', '5');
INSERT INTO HasCompleted 
  VALUES 
    ('hjorthjort', 'TDA357', '5');
INSERT INTO HasCompleted 
  VALUES 
    ('hjorthjort', 'TDA550', '5');
INSERT INTO HasCompleted
  VALUES
    ('hjorthjort', 'DAT216', '3');
INSERT INTO HasCompleted
  VALUES
    ('hjorthjort', 'TDA545', '5');
INSERT INTO HasCompleted
  VALUES
    ('databar', 'TDA550', 'U');
INSERT INTO HasCompleted
  VALUES
    ('foobar', 'TDA550', '3');

