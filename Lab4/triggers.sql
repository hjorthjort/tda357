-- ********************************
-- ******* REGISTRATION ***********
-- ********************************

CREATE OR REPLACE TRIGGER CourseRegistration
INSTEAD OF INSERT ON Registrations
FOR EACH ROW
DECLARE
  hasPassed Int; 
  isRegistered Int;
  registeredStudents Int;
  isLimited Int;
  maxStudents Int;
  untakenPrereqs Int;
BEGIN
  SELECT COUNT (*) INTO hasPassed FROM HasCompleted 
    WHERE course  = :new.course AND student = :new.student;
   -- Has student already passed?
  IF hasPassed > 0 THEN 
     RAISE_APPLICATION_ERROR(-20001,'Student has already passed');
  END IF; -- Student has not already passed the course
  
  -- Is student already registered or waiting?
  SELECT COUNT(*) INTO isRegistered FROM Registrations WHERE student = :new.student AND course = :new.course;
  IF isRegistered > 0 THEN
    RAISE_APPLICATION_ERROR(-20002,'Student is already registered or waiting');
  END IF;
  
   -- Has student passed prerequisites?
  SELECT COUNT(*) INTO untakenPrereqs FROM (
    (SELECT prereq FROM IsPrereqFor WHERE course = :new.course )
      MINUS 
    (SELECT course FROM HasCompleted WHERE student = :new.student));
  IF untakenPrereqs > 0 THEN
    RAISE_APPLICATION_ERROR(-20003,'Student lacks prerequisites');
  END IF;
  -- Student has taken all prerequisites
  
   -- Is course Limited?
  SELECT COUNT(*) INTO isLimited FROM LimitedCourses WHERE code = :new.course;
  IF isLimited = 0  THEN
     INSERT INTO IsRegisteredFor VALUES(:new.student, :new.course);
     -- Student registered! Sweeeeeet!
  ELSE
      -- Is there room in the course?
      SELECT COUNT(*) INTO registeredStudents FROM Registrations
        WHERE course = :new.course AND status = 'registered';
        
      SELECT max_students INTO maxStudents FROM LimitedCourses
        WHERE code = :new.course;
      IF registeredStudents >= maxStudents THEN
        INSERT INTO IsWaitingFor VALUES (:new.student, :new.course, CURRENT_TIMESTAMP);
        --Student is on waiting list!
      ELSE 
        --Student is registered!
        INSERT INTO IsRegisteredFor VALUES(:new.student, :new.course);
      END IF;
   END IF;
END;
/

-- ********************************
-- ******* UNREGISTRATION *********
-- ********************************

CREATE OR REPLACE TRIGGER CourseUnregistration
INSTEAD OF DELETE ON Registrations
FOR EACH ROW
DECLARE
isRegistered Int;
isLimited Int;
registeredStudents Int;
maxStudents Int;
waitingStudents Int;
nextWaiting VARCHAR(255);
BEGIN

  -- Delete student from both waiting list and registration list. 
  -- They should only be in one of these.
  DELETE FROM IsWaitingFor WHERE student = :old.student AND course = :old.course;
  DELETE FROM IsRegisteredFor WHERE student = :old.student AND course = :old.course;

  -- See if there is now room on the course.
  
  -- Is course Limited?
  SELECT COUNT(*) INTO isLimited FROM LimitedCourses WHERE code = :old.course;
  IF isLimited > 0  THEN
      --It is limited.
      
      -- Is there room in the course?
      SELECT COUNT(*) INTO registeredStudents FROM Registrations
        WHERE course = :old.course AND status = 'registered';
      SELECT max_students INTO maxStudents FROM LimitedCourses
        WHERE code = :old.course;
      IF registeredStudents < maxStudents THEN
        -- There is room!
        
        -- Is anyone waiting?
         -- Is there room in the course?
        SELECT COUNT(*) INTO waitingStudents FROM Registrations
          WHERE course = :old.course AND status = 'waiting';
          IF waitingStudents > 0 THEN
            -- There are students waiting
            
              -- Find next student to insert
              SELECT student INTO nextWaiting FROM CourseQueuePositions WHERE course = :old.course AND positionInQueue = 1;
              -- Delete them from waiting list
              DELETE FROM IsWaitingFor WHERE course = :old.course AND student = nextWaiting;
              -- Insert them into waiting students.
              INSERT INTO Registrations (student, course) VALUES (nextWaiting, :old.course);
              -- By only inserting via Registrations, we know that a student on the waiting list must be eligeble for course, unless overridden by admin
          END IF;
      END IF;
   END IF;
END;
/